<?php
session_start();
include 'header.php';
require 'connection.php';
if (isset($_POST["submit"])) {
    $name = $_POST["name"];
    $price = $_POST["price"];
    if ($_FILES["image"]["error"] === 4) {
        echo
        "<script>
        alert('Please select an image');
        window.location.href='upload.php';
        </script>";
    } else {
        $fileName = $_FILES["image"]["name"];
        $fileSize = $_FILES["image"]["size"];
        $tmpName = $_FILES["image"]["tmp_name"];
        $validImageExtension = ['.jpg', '.jpeg', '.png'];
        $imageExtension = explode('.', $fileName);
        $imageExtension = strtolower(end($imageExtension));
        if ($fileSize > 1000000) {
            echo
            "<script>
        alert('Image Size too Large');
        </script>";
        } else {
            $newImageName = uniqid() . $imageExtension;
            move_uploaded_file($tmpName, 'img/'.  $newImageName);

            // $newImageName = uniqid() . $imageExtension;
            // $destination = 'img/' . $newImageName; // Combine directory and filename
            // move_uploaded_file($tmpName, $destination);            

            $query = "INSERT INTO cart_item VALUES ('', '$newImageName', '$name', '$price')";
            mysqli_query($conn, $query);
            echo
            "<script>
        alert('UPLOADED');
        document.location.href='index.php';
        </script>";
        }
    }
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product Form</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <style>
        body {
            background-color: #f8f9fa;
        }

        .form-container {
            background-color: #ffffff;
            padding: 40px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
        }

        .form-title {
            text-align: center;
            margin-bottom: 30px;
            font-size: 24px;
            color: #333333;
        }

        .form-group {
            margin-bottom: 25px;
        }

        .form-label {
            color: #555555;
        }

        .form-control {
            border-radius: 5px;
            border-color: #ced4da;
        }

        .form-control:focus {
            border-color: #80bdff;
            box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25);
        }

        .form-btn {
            background-color: #007bff;
            border-color: #007bff;
            color: #ffffff;
        }

        .form-btn:hover {
            background-color: #0056b3;
            border-color: #0056b3;
        }
    </style>
</head>

<body>
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="form-container">
                    <h2 class="form-title">Product Information</h2>
                    <form method="post" action="" autocomplete="off" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="name" class="form-label">Name of the product:</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name" required value="">
                        </div>
                        <div class="form-group">
                            <label for="price" class="form-label">Price:</label>
                            <input type="text" class="form-control" name="price" id="price" placeholder="Enter price" required value="">
                        </div>
                        <div class="form-group">
                            <label for="image" class="form-label">Image of the Product:</label>
                            <input type="file" class="form-control-file" name="image" id="image" accept=".jpg, .jpeg, .png" value="">
                        </div>
                        <div class="d-flex justify-content-center">
                            <div class="mr-2">
                                <button type="submit" class="btn btn-primary form-btn" name="submit">Submit</button>
                            </div>
                            <div>
                            <button type="button" class="btn btn-primary form-btn" id="backBtn" name="back">Back</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php include 'footer.php'; ?>
</body>

</html>

<script>
    $(document).ready(function() {
        // Attach click event handler to the Back button
        $('#backBtn').on('click', function() {
            // Redirect to index.php
            window.location.href = 'index.php';
        });
    });
</script>