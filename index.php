<?php
session_start();
include 'header.php';

$connect = mysqli_connect("localhost", "root", "", "sunil");

if (isset($_POST['add_to_cart'])) {
    if (isset($_SESSION['cart'])) {
        $session_array_id = array_column($_SESSION['cart'], 'id');
        if (!in_array($_GET['id'], $session_array_id)) {
            $session_array = array(
                'id' => $_GET['id'],
                'name' => $_POST['name'],
                'price' => $_POST['price'],
                'quantity' => $_POST['quantity'],
            );
            $_SESSION['cart'][] = $session_array;
        }
    } else {
        $session_array = array(
            'id' => $_GET['id'],
            'name' => $_POST['name'],
            'price' => $_POST['price'],
            'quantity' => $_POST['quantity'],
        );
        $_SESSION['cart'][] = $session_array;
    }
}

if (isset($_GET['action'])) {

    if ($_GET['action'] == 'clearall') {
        unset($_SESSION['cart']);
    }

    if ($_GET['action'] == 'remove') {
        foreach ($_SESSION['cart'] as $key => $value) {
            if ($value['id'] == $_GET['id']) {
                unset($_SESSION['cart'][$key]);
            }
        }
    }
}

  echo isset($_SESSION['cart']) ? count($_SESSION['cart']) : 0;
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shopping Cart</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">

    <style>

        .center-content {
            text-align: center;
        }

        .product-images {
            display: block;
            margin-left: auto;
            margin-right: auto;
            height: auto; 
        }

        .center-input {
            display: block;
            margin: 0 auto;
            width: 30%;
            border: 1px solid #555;
        }

        .add-to-cart-container {
            display: flex;
            align-items: flex-start; 
            flex-wrap: wrap; 
        }

        .product-box {
            padding: 15px;
        }

        .add-to-cart-btn {
            margin: 20px; 
        }

    </style>

</head>

<body>

    <!-- <div class="container-fluid">
        <div class="col-md-12">
            <div class="row add-to-cart-container">

                <?php
                /*
                    $query = "SELECT * FROM cart_item";

                    if(isset($_GET['search'])){
                        $search = $_GET['search'];
                        $query = "SELECT * FROM cart_item WHERE name LIKE 'J%'";
                    }

                    $result = mysqli_query($connect, $query);

                    while ($row = mysqli_fetch_array($result)) { ?>
                        <div class="col-md-3 p-5 product-box">
                            <form method="post" action="index.php?id=<?= $row['id'] ?>">
                                <div class="center-content">
                                    <img src="img/<?= $row['image'] ?>" style="height: 250px; width: 300px;" class="product-images">
                                </div>
                                <h5 class="text-center"><?= $row['name']; ?></h5>
                                <h5 class="text-center">Rs.<?= number_format($row['price'], 2); ?></h5>
                                <input type="hidden" name="name" value="<?= $row['name'] ?>">
                                <input type="hidden" name="price" value="<?= $row['price'] ?>">
                                <input type="number" name="quantity" class="form-control center-input" value="1">
                                <div class="d-flex justify-content-center">
                                    <input type="submit" name="add_to_cart" class="btn btn-warning btn-block m-2 add-to-cart-btn" value="Add to Cart">
                                </div>
                            </form>
                        </div>
                    <?php }
                  */  
                ?>

                <div id="search_results_container">                    
                </div>
            </div>
        </div>                        
        
    </div> -->

    <div id="search_results_container">                    
    </div>

    <?php 
        include 'body.php'; 
     ?> 

    <?php 
        include 'footer.php'; 
    ?>
    
</body>

</html>

<!-- <script>
$(document).ready(function() {
    $('#addToCartForm').submit(function(event) {
        event.preventDefault();
        var formData = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: 'index.php',
            data: formData,
            success: function(response) {
                alert('Item added to cart successfully!');
            },
            error: function(xhr, status, error) {
                alert('Error adding item to cart. Please try again.');
            }
        });
    });
});
</script> -->