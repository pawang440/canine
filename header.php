<?php
// Start the session (if not already started)
// session_start();

// Check if the username is set in the session
if(isset($_SESSION['user_name'])) {
    // Get the first letter of the username
    $userInitial = strtoupper(substr($_SESSION['user_name'], 0, 1));
} else {
    // Default initial if username is not set
    $userInitial = 'UAA'; // Or any default initial you prefer
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <title>Your Website Title</title>
    <style>

        body {
            margin: 0;
            padding-top: 50px;
        }

        #header {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 50px;
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 10px;
            background-color: #1a365d;
            z-index: 1000;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }

        .round-button {
            width: 40px;
            height: 40px;
            border-radius: 50%;
            background-color: #007bff;
            color: #fff;
            text-align: center;
            line-height: 40px;
            cursor: pointer;
            display: inline-flex;
            align-items: center;
            justify-content: center;
            text-decoration: none;
            font-size: 18px;
            font-weight: bold;
            margin-left: 10px;
        }

        .dropdown {
            position: relative;
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 120px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
        }

        .dropdown-content a {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }

        .dropdown-content a:hover {
            background-color: #f1f1f1;
        }

        .dropdown:hover .dropdown-content {
            display: block;
        }

        #home-link {
            color: #fff;
            text-decoration: none;
            margin-left: 20px;
            font-size: 20px;
        }

        #home-link:hover {
            text-decoration: underline;
            /* font-size: 18px; */
        }

        #home-link:active {
            color: #ccc;
        }

        #home-link:focus {
            outline: none;
            /* font-size: 18px; */
        }

        #home-link:hover,
        #home-link:focus {
            cursor: pointer; 
        }

        .search-container {
            margin-left: auto;
            margin-right: 20px;
            display: flex;
            align-items: center;
        }

        .search-label {
            margin-right: 10px;
            color: #fff;
            
        }

        .search-input {
            padding: 5px;
            border: 1px solid #ccc;
            border-radius: 5px;
            width: 200px;
        }

        .cart-button {
            margin-left: 20px;
            color: #fff;
            padding: 8px 12px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            text-decoration: none;
            font-size: 16px;
        }
    </style>
</head>
<body>
    <div id="header">
        <div class="dropdown">
            <a href="#" class="round-button">
                <span><?php echo $userInitial; ?></span>
            </a>
            <div class="dropdown-content">
                <a href="login.php">Logout</a>
                <a href="upload.php">Add Item</a>
            </div>
        </div>
        <a id="home-link">Home</a>
        <div class="search-container">
            <input type="text" id="search" name="search" class="search-input" placeholder="Search...">
            <a href="orders.php" class="cart-button">
                <i class="fas fa-shopping-cart"></i>
                <span id="cart-counter" class="cart-counter">
                    <?php echo isset($_SESSION['cart']) ? count($_SESSION['cart']) : 0; ?>
                </span>
            </a>
        </div>
    </div>
</body>
</html>

<script>

$(document).ready(function () {
    $('#home-link').click(function (e) {
        e.preventDefault();
        window.location.href = 'index.php'; 
    });

    $('#search').keyup(function() {        
        var searchText = $(this).val();
        
        if (searchText === ''){

            $.ajax({
            type: 'GET',
            url: 'body.php',
            data: {search: searchText },
            success: function(response) {
                $('#search_results_container').hide();
                $('.product-box').show();
            }
            }); 


        }
        else{
            $.ajax({
            type: 'GET',
            url: 'body.php',
            data: {search: searchText },
            success: function(response) {
                $('#search_results_container').show();
                $('#search_results_container').html(response);
                $('.product-box').hide();
            }
        });   
        }
    });
});

</script>

<script src="functions.js"></script>

