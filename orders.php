<?php
session_start();
include 'header.php';

if (isset($_GET['action'])) {

    if ($_GET['action'] == 'clearall') {
        unset($_SESSION['cart']);
    }

    if ($_GET['action'] == 'remove') {
        foreach ($_SESSION['cart'] as $key => $value) {
            if ($value['id'] == $_GET['id']) {
                unset($_SESSION['cart'][$key]);
            }
        }
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
            padding-top: 40px;
        }

        h2 {
            text-align: center;
            margin-bottom: 20px;
        }

        .table-container {
            margin: 0 auto;
            width: 100%;
            max-width: 800px; /* Adjust max-width as needed */
        }

        .table {
            width: 100%;
            margin-bottom: 20px;
            border-collapse: collapse;
        }

        .table th, .table td {
            border: 1px solid #ccc;
            padding: 8px;
            text-align: center;
        }

        .table th {
            background-color: #f2f2f2;
        }

        .table td.total {
            font-weight: bold;
        }

        .action-btn {
            text-align: center;
        }

        .action-btn button {
            width: 100px;
        }

        @media(max-width: 768px) {
            .table td, .table th {
                padding: 5px;
                font-size: 12px;
            }
            .action-btn button {
                width: 80px;
                font-size: 12px;
            }
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="col-md-8 mx-auto">
            <h2>Order Confirmation</h2>
            <div class="table-container">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Sno</th>
                            <th>Item Name</th>
                            <th>Item Price</th>
                            <th>Item Quantity</th>
                            <th>Total Price</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $total = 0;
                        $sno = 1;
                        if (!empty($_SESSION['cart'])) {
                            foreach ($_SESSION['cart'] as $key => $value) {
                                $total = $value['quantity'] * $value['price'];
                        ?>
                                <tr>
                                    <td><?= $sno++; ?></td>
                                    <!-- <td><-?= $value['id']; ?></td>  -->
                                    <td><?= $value['name']; ?></td>
                                    <td><?= $value['price']; ?></td>
                                    <td><?= $value['quantity']; ?></td>
                                    <td><?= number_format($value['price'] * $value['quantity'], 2); ?></td>
                                    <td class="action-btn">
                                        <a href="orders.php?action=remove&id=<?= $value['id']; ?>">
                                        <button class="btn btn-danger">Remove</button> </a>
                                    </td>
                                </tr>
                        <?php
                            }
                        ?>
                            <tr>
                                <td colspan="4"></td>
                                <td class="total"><b>Total Price</b></td>
                                <td class="action-btn">
                                    <a href="index.php?action=clearall">
                                        <button class="btn btn-warning">Order All</button>
                                    </a>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php include 'footer.php'; ?>
</body>
</html>

<!-- <script>
$(document).ready(function(){
    $('.remove-item-btn').click(function(e){
        e.preventDefault();

        if(confirm('Are you sure you want to remove this item?')) {
            var itemID = $(this).data('itemid'); // Get item ID from data attribute

            $.ajax({
                type: 'GET',
                url: 'functions.php', 
                data: { action: 'remove', id: itemID },
                success: function(response) {
                    alert('Item removed successfully!');
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText); // Log server response
        console.log(status); // Log status
        console.log(error); // Log error
                    alert('Error removing item. Please try again.');
                }
            });
        }
    });
});
</script> -->
