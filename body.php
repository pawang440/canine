<?php
$connect = mysqli_connect("localhost", "root", "", "sunil");


if(isset($_GET['search'])) {
    $search = $_GET['search'];
    $query = "SELECT * FROM cart_item WHERE name LIKE '$search%'";
    $result = mysqli_query($connect, $query);
    $page = '<div class="container-fluid">
             <div class="col-md-12">
             <div class="row add-to-cart-container">';
    // Display search results
    while ($row = mysqli_fetch_array($result)) {
         $page .= '<div class="col-md-3 p-5 search-product-box">
            <form method="post" action="index.php?id=' . $row['id'] . '">
                <div class="center-content">
                    <img src="img/' . $row['image'] . '" style="height: 250px; width: 300px;" class="product-images">
                </div>
                <h5 class="text-center">' . $row['name'] . '</h5>
                <h5 class="text-center>Rs.<?= number_format('.$row['price'].', 2); ?></h5>
                <input type="hidden" name="name" value="' . $row['name'] . '">
                <input type="hidden" name="price" value="' . $row['price'] . '">
                <input type="number" name="quantity" class="form-control center-input" value="1">
                <div class="d-flex justify-content-center">
                    <input type="submit" name="add_to_cart" class="btn btn-warning btn-block m-2 add-to-cart-btn" value="Add to Cart">
                </div>
            </form>
        </div>';
    }

    $page .='</div>
             </div>
             </div>';
    // echo "abc";
    echo $page;
}
else {
    // echo "loremasdifohsij sk fgslf hsi hijhgiudgfakg hpigh sghjgkjashgkjd gbkae hik;gkjsdjg sdgh";
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shopping Cart</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">

    <style>

        .center-content {
            text-align: center;
        }

        .product-images {
            display: block;
            margin-left: auto;
            margin-right: auto;
            height: auto; 
        }

        .center-input {
            display: block;
            margin: 0 auto;
            width: 30%;
            border: 1px solid #555;
        }

        .add-to-cart-container {
            display: flex;
            align-items: flex-start; 
            flex-wrap: wrap; 
        }

        .product-box {
            padding: 15px;
        }

        .search-product-box {
            padding: 15px;
        }

        .add-to-cart-btn {
            margin: 20px; 
        }

    </style>

</head>

<body>
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="row add-to-cart-container">
                <?php                
                    $query = "SELECT * FROM cart_item";

                    // if(isset($_GET['search'])){
                    //     $search = $_GET['search'];
                    //     $query = "SELECT * FROM cart_item WHERE name LIKE 'J%'";
                    // }

                    $result = mysqli_query($connect, $query);

                    while ($row = mysqli_fetch_array($result)) { ?>
                        <div class="col-md-3 p-5 product-box">
                            <!-- <form method="post" action="index.php?id=<-?= $row['id'] ?>"> -->
                            <form method="post" action="index.php">
                                <div class="center-content">
                                    <img src="img/<?= $row['image'] ?>" style="height: 250px; width: 300px;" class="product-images">
                                </div>                                
                                <h5 class="text-center"><?= $row['name']; ?></h5>
                                <h5 class="text-center">Rs.<?= number_format($row['price'], 2); ?></h5>
                                <input type="hidden" name="id" value="<?= $row['id'] ?>">
                                <input type="hidden" name="name" value="<?= $row['name'] ?>">
                                <input type="hidden" name="price" value="<?= $row['price'] ?>">
                                <input type="number" name="quantity" class="form-control center-input" value="1">
                                <div class="d-flex justify-content-center">
                                    <!-- <input type="submit" name="add_to_cart" class="btn btn-warning btn-block m-2 add-to-cart-btn" value="Add to Cart"> -->
                                    <button type="submit" name="add_to_cart">Add to Cart</button>
                                </div>
                            </form>
                        </div>
                    <?php }
                    
                ?>
            </div>
        </div>                        
    </div>
</body>

</html>

<script src="functions.js"></script>